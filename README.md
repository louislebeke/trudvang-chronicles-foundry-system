# Trudvang Chronicles RPG System

## Description

A game system to play to Trudvang Chronicles RPG. 

The system includes:

* Character sheet
* Items sheet (Spell, Divine power, Weapon, Armour, Item, Shield).

### Character Sheet

The character sheet contains all existing skills. Vitner and Faith skills are hidden until the character has a Vitner or Faith tablet.

Each skill name is clickable and allows to launch a test on the clicked skill.

The basic initiative is calculated according to the character's equipment.

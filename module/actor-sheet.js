import { EntitySheetHelper } from "./helper.js";
import { ATTRIBUTE_TYPES } from "./constants.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SimpleActorSheet extends ActorSheet {

  modifiers() {
    return [
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.almost-impossible'),
        value: -15
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.extremely-difficult'),
        value: -10
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.very-difficult'),
        value: -5
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.difficult'),
        value: -3
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.hard'),
        value: -1
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.normal'),
        value: 0
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.simple'),
        value: 1
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.easy'),
        value: 3
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.very-easy'),
        value: 5
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.extremely-easy'),
        value: 10
      },
      {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifiers.almost-unmissable'),
        value: 15
      }
    ]
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["trudvang", "sheet", "actor"],
      template: "systems/trudvang/templates/actor-sheet.html",
      width: 900,
      height: 800,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}],
      scrollY: [".biography", ".items", ".attributes"],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData() {
    const context = super.getData();
    EntitySheetHelper.getAttributeData(context.data);
    context.shorthand = !!game.settings.get("trudvang", "macroShorthand");
    context.dtypes = ATTRIBUTE_TYPES;

    // define Health and fear state
    const { data } = context.data
    const { psyche } = data.traits;
    const { health, fear, persistenceInWild } = data.secondaryTraits;
    this._determineSV(data.skills);
    this._determineBattlePoints(data.skills.battle, data.battlePoints);
    this._determineBattleAttributes(data.battleAttributes, data.skills.battle, data.traits.dexterity.value, data.skills.agility);
    this._determineFaithAttributes(data.faithAttributes, data.skills.faith);
    this._determineVitnerAttributes(data.vitnerAttributes, data.skills.vitner);
    const { survival } = data.skills.wild.disciplines;
    persistenceInWild.value = this._determinePersistenceInWild(psyche.value, survival.lvl, survival.specialities.hardened.lvl);
    const newHealth = this.defineHealthState(health.value, health.maxValue);
    const newFear = this.defineFearState(fear.value);
    health.state = newHealth.state;
    health.modifier = newHealth.modifier;
    fear.state = newFear.state;
    fear.modifier = newFear.modifier;

    // Translations
    for (let [key, personalAbil] of Object.entries(data.personalData)){
      personalAbil.label = game.i18n.localize(`TRUDVANG.description-tab.${key}`);
    }
    for (let [key, trait] of Object.entries(data.traits)){
      trait.label = game.i18n.localize(`TRUDVANG.traits.${key}`);
    }
    for (let [key, secondaryTrait] of Object.entries(data.secondaryTraits)){
      secondaryTrait.label = game.i18n.localize(`TRUDVANG.secondaryTraits.${key}`);
    }
    for (let [key, battlePoint] of Object.entries(data.battlePoints)){
      battlePoint.label = game.i18n.localize(`TRUDVANG.battlePoints.${key}`);
      if (battlePoint.specialisations) {
        for (let [speKey, spe] of Object.entries(battlePoint.specialisations)){
          spe.label = game.i18n.localize(`TRUDVANG.battlePoints.${speKey}`);
        }
      }
    }
    for (let [key, battleAttribute] of Object.entries(data.battleAttributes)){
      battleAttribute.label = game.i18n.localize(`TRUDVANG.battleAttributes.${key}`);
    }

    for (let [key, faithAttribute] of Object.entries(data.faithAttributes)) {
      if (faithAttribute.label) {
        faithAttribute.label = game.i18n.localize(`TRUDVANG.faithAttributes.${key}`)
      } else {
        for (let [attrKey, attribute] of Object.entries(data.faithAttributes[key])) {
          attribute.label = game.i18n.localize(`TRUDVANG.faithAttributes.${key}.${attrKey}`);
        }
      }
    }

    for (let [key, vitnerAttribute] of Object.entries(data.vitnerAttributes)) {
      if (vitnerAttribute.label) {
        vitnerAttribute.label = game.i18n.localize(`TRUDVANG.vitnerAttributes.${key}`)
      } else {
        for (let [attrKey, attribute] of Object.entries(data.vitnerAttributes[key])) {
          attribute.label = game.i18n.localize(`TRUDVANG.vitnerAttributes.${key}.${attrKey}`);
        }
      }
    }
    health.state = game.i18n.localize(`TRUDVANG.healthState.${health.state}`);
    fear.state = game.i18n.localize(`TRUDVANG.fearState.${fear.state}`);
    context.systemData = context.data.data;
    const { items } = context.data;
    const tabletsInfo = this._getTablets(items);
    context.systemData.tablets = tabletsInfo.tablets;
    context.systemData.hasSpells = tabletsInfo.spells;
    context.systemData.hasDivinePower = tabletsInfo.divinePower;
    context.systemData.armours = this._getItems(items, 'armour');
    context.systemData.shields = this._getItems(items, 'shield');
    context.systemData.weapons = this._getItems(items, 'weapon');
    context.systemData.items = this._getItems(items, 'item');
    this._calculateInitiativeWithItems(data.battleAttributes.basicInitiative, context.systemData.armours, context.systemData.weapons, context.systemData.shields);
    data.initiative.value = data.battleAttributes.basicInitiative.value;
    context.systemData.secondaryTraits.move.valueUpdated = context.systemData.secondaryTraits.move.value;
    this._calculateNewMovement(context.systemData.secondaryTraits.move, context.systemData.armours);
    console.log(data);
    return context;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if ( !this.isEditable ) return;

    // Rollable traits
    html.find('.rollable__situation').click(this._onRollSituation.bind(this));

    // Rollable skill
    html.find('.rollable__skill').click(this._onRoll.bind(this));

    // Rollable battleActions
    html.find('.rollable__battleActions').click(this._onRoll.bind(this));

    // Attribute Management
    html.find(".attributes").on("click", ".attribute-control", EntitySheetHelper.onClickAttributeControl.bind(this));
    html.find(".groups").on("click", ".group-control", EntitySheetHelper.onClickAttributeGroupControl.bind(this));
    html.find(".attributes").on("click", "a.attribute-roll", EntitySheetHelper.onAttributeRoll.bind(this));

    // Rollable damage
    html.find(".rollable__damage").click((this._onRollDamage.bind(this)));

    // Item Controls
    html.find(".item-control").click(this._onItemControl.bind(this));
    html.find(".power-control").click(this._onPowerControl.bind(this));
    html.find(".items .rollable").on("click", this._onItemRoll.bind(this));

    // Add draggable for Macro creation
    html.find(".attributes a.attribute-roll").each((i, a) => {
      a.setAttribute("draggable", true);
      a.addEventListener("dragstart", ev => {
        let dragData = ev.currentTarget.dataset;
        ev.dataTransfer.setData('text/plain', JSON.stringify(dragData));
      }, false);
    });
  }

  /* -------------------------------------------- */

  /**
   * Handle click events for Item control buttons within the Actor Sheet
   * @param event
   * @private
   */
  _onItemControl(event) {
    event.preventDefault();

    // Obtain event data
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    // Handle different actions
    switch ( button.dataset.action ) {
      case "create":
        const cls = getDocumentClass("Item");
        return cls.create({name: game.i18n.localize("TRUDVANG.ItemNew"), type: "item"}, {parent: this.actor});
      case "edit":
        return item.sheet.render(true);
      case "delete":
        return item.delete();
    }
  }

  _onPowerControl(event) {
    event.preventDefault();

    // Obtain event data
    const button = event.currentTarget;
    const element = button.closest(".power");
    const power = this.actor.items.get(element?.dataset.itemId);

    // Handle different actions
    switch ( button.dataset.action ) {
      case "create":
        const cls = getDocumentClass("Item");
        return cls.create({name: game.i18n.localize("TRUDVANG.ItemNew"), type: "divinePower"}, {parent: this.actor});
      case "edit":
        return power.sheet.render(true);
      case "delete":
        return power.delete();
    }
  }

  /* -------------------------------------------- */

  /**
   * Listen for roll buttons on items.
   * @param {MouseEvent} event    The originating left click event
   */
  _onItemRoll(event) {
    let button = $(event.currentTarget);
    const li = button.parents(".item");
    const item = this.actor.items.get(li.data("itemId"));
    let r = new Roll(button.data('roll'), this.actor.getRollData());
    return r.toMessage({
      user: game.user.id,
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: `<h2>${item.name}</h2><h3>${button.text()}</h3>`
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _getSubmitData(updateData) {
    let formData = super._getSubmitData(updateData);
    formData = EntitySheetHelper.updateAttributes(formData, this.object);
    formData = EntitySheetHelper.updateGroups(formData, this.object);
    return formData;
  }

  /**
   *
   * @param healthPoints
   * @param healthMax
   * @returns {{modifier: number, state: string}}
   */
  defineHealthState(healthPoints, healthMax) {
    // define levels
    const level = healthMax / 4;
    const levels = {
      scratched: Math.ceil(level),
      wounded: level * 2,
      seriouslyWounded: level * 3,
      fatallyWounded: healthMax
    }
    // get decimal 0.00, 0.25, 0.5 or 0.75
    const decimal = level % 1;
    if (decimal === 0.75) {
      levels.wounded = Math.ceil(level) + levels.scratched;
      levels.seriouslyWounded = Math.ceil(level) + levels.wounded;
    } else if (decimal === 0.5) {
      levels.wounded = Math.ceil(level) + levels.scratched;
      levels.seriouslyWounded = Math.floor(level) + levels.wounded;
    } else if (decimal === 0.25) {
      levels.wounded = Math.floor(level) + levels.scratched;
      levels.seriouslyWounded = Math.floor(level) + levels.wounded;
    }
    const healthPointsRemoved = healthMax - healthPoints;
    // define state
    let modifier = 0;
    const state = Object.keys(levels).find((key, i) => {
      if (healthPointsRemoved <= levels[key]) {
        if (i === 1) modifier = -1;
        if (i === 2) modifier = -3;
        if (i === 3) modifier = -7;
      }
      return healthPointsRemoved <= levels[key];
    });
    return {
      state,
      modifier
    };
  }

  /**
   *
   * @param fearPoints
   * @returns {{modifier: number, state: string}}
   */
  defineFearState(fearPoints) {
    const levels = {
      madness: 51,
      petrified: 41,
      terrified: 31,
      horrified: 21,
      scared: 11,
      anxious: 0,
    }
    let modifier = 0;
    const state = Object.keys(levels).find((key, i) => {
      if (levels[key] <= fearPoints) {
        if (i <= 1) modifier = -7;
        if (i === 2) modifier = -5;
        if (i === 3) modifier = -3;
        if (i === 4) modifier = -1;
        if (i === 5) modifier = 0;
      }
      return levels[key] <= fearPoints;
    });
    return {
      state,
      modifier
    };
  }

  /**
   *
   * @param psy
   * @param survivalLvl
   * @param hardenedLvl
   * @returns {*}
   * @private
   */
  _determinePersistenceInWild(psy, survivalLvl, hardenedLvl) {
    return 10 + parseInt(psy, 10) + survivalLvl + (hardenedLvl * 2);
  }

  /**
   *
   * @param skills
   * @private
   */
  _determineSV(skills) {
    for (let [key, skill] of Object.entries(skills)) {
      const baseSV = skill.value;
      skill.label = game.i18n.localize(`TRUDVANG.skills.${key}.label`);
      for (let [disciplineKey, discipline] of Object.entries(skill.disciplines)) {
        discipline.value = baseSV + discipline.lvl;
        discipline.label = game.i18n.localize(`TRUDVANG.skills.${key}.${discipline.translateKey}`);
        for (let [specialityKey, speciality] of Object.entries(discipline.specialities)) {
          if (speciality.handOption) {
            speciality.valueLeft = discipline.value + (speciality.lvlLeft * 2);
            speciality.valueRight = discipline.value + (speciality.lvlRight * 2);
            speciality.labelLeft = game.i18n.format(`TRUDVANG.skills.${key}.${speciality.translateKey}`, { hand: game.i18n.localize('TRUDVANG.left') });
            speciality.labelRight = game.i18n.format(`TRUDVANG.skills.${key}.${speciality.translateKey}`, { hand: game.i18n.localize('TRUDVANG.right') });
          } else {
            speciality.value = discipline.value + (speciality.lvl * 2);
            speciality.label = game.i18n.localize(`TRUDVANG.skills.${key}.${speciality.translateKey}`);
          }
        }
      }
    }
  }

  /**
   *
   * @param battle
   * @param battlePoints
   * @private
   */
  _determineBattlePoints(battle, battlePoints) {
    battlePoints.free.value = battle.value + battle.disciplines.combatExperience.lvl;
    battlePoints.battleActions.value = battle.disciplines.combatExperience.specialities.battleActions.lvl * 2;
    battlePoints.attack.value = battle.disciplines.combatExperience.specialities.fighter.lvl * 2;
    battlePoints.hand.value = battle.disciplines.unarmedCombat.lvl;
    battlePoints.hand.specialisations.brawl.value = battle.disciplines.unarmedCombat.specialities.brawl.lvl * 2;
    battlePoints.hand.specialisations.wrestling.value = battle.disciplines.unarmedCombat.specialities.wrestling.lvl * 2;
    battlePoints.armed.value = battle.disciplines.armedCombat.lvl;
    battlePoints.armed.specialisations.crossbow.value = battle.disciplines.armedCombat.specialities.crossbow.lvl * 2;
    battlePoints.armed.specialisations.bow.value = battle.disciplines.armedCombat.specialities.bow.lvl * 2;
    battlePoints.armed.specialisations.twoHands.value = battle.disciplines.armedCombat.specialities.twoHand.lvl * 2;
    battlePoints.armed.specialisations.throwingWeaponLeft.value = battle.disciplines.armedCombat.specialities.throw.lvlLeft * 2;
    battlePoints.armed.specialisations.throwingWeaponRight.value = battle.disciplines.armedCombat.specialities.throw.lvlRight * 2;
    battlePoints.armed.specialisations.lightLeft.value = battle.disciplines.armedCombat.specialities.light.lvlLeft * 2;
    battlePoints.armed.specialisations.lightRight.value = battle.disciplines.armedCombat.specialities.light.lvlRight * 2;
    battlePoints.armed.specialisations.heavyLeft.value = battle.disciplines.armedCombat.specialities.heavy.lvlLeft * 2;
    battlePoints.armed.specialisations.heavyRight.value = battle.disciplines.armedCombat.specialities.heavy.lvlRight * 2;
    battlePoints.armed.specialisations.shield.value = battle.disciplines.armedCombat.specialities.shield.lvl * 2;
  }

  /**
   *
   * @param battleAttributes
   * @param battle
   * @param dexterity
   * @param agility
   * @private
   */
  _determineBattleAttributes(battleAttributes, battle, dexterity, agility) {
    battleAttributes.basicInitiative.value =
      parseInt(dexterity, 10) + battle.disciplines.combatExperience.lvl + (battle.disciplines.combatExperience.specialities.battleReactions.lvl * 2);
    battleAttributes.battleWithShieldHand.value = -15 + agility.disciplines.bodyControl.lvl + (agility.disciplines.bodyControl.specialities.ambidextrous.lvl * 2);
    battleAttributes.mountedBattle.value = 15 - (agility.disciplines.equestrianMastery.specialities.riding.lvl * 3);
    battleAttributes.dodge.value = agility.disciplines.combatManoeuvre.specialities.dodge.value;
  }

  /**
   *
   * @param faithAttributes
   * @param faith
   * @private
   */
  _determineFaithAttributes(faithAttributes, faith) {
    faithAttributes.divineReserve.faith.value = faith.value;
    faithAttributes.divineReserve.divinePowers.value = faith.disciplines.divinePowers.lvl * 3;
    faithAttributes.divineReserve.devotion.value = faith.disciplines.divinePowers.specialities.devotion.lvl * 7;
    faithAttributes.divineReserve.power.value = faith.disciplines.divinePowers.specialities.powerful.lvl * 7;
    faithAttributes.divineReserve.total.value =
      faithAttributes.divineReserve.faith.value + faithAttributes.divineReserve.divinePowers.value + faithAttributes.divineReserve.devotion.value + faithAttributes.divineReserve.power.value;
    faithAttributes.divineInvocation.faith.value = faith.value;
    faithAttributes.divineInvocation.invocation.value = faith.disciplines.invocation.lvl;
    faithAttributes.divineInvocation.speciality.value = faith.disciplines.invocation.specialities.speciality.lvl * 2;
    faithAttributes.divineInvocation.total.value = faithAttributes.divineInvocation.faith.value + faithAttributes.divineInvocation.invocation.value + faithAttributes.divineInvocation.speciality.value;
    faithAttributes.divineInvocation.rigorousBonus.value = Math.floor(faith.disciplines.religiousConcentration.specialities.rigorous.lvl * 2);
    faithAttributes.efficiency.concentration.value = faith.disciplines.religiousConcentration.lvl + (faith.disciplines.religiousConcentration.specialities.serenity.lvl * 2);
    faithAttributes.efficiency.fumble.value = faithAttributes.efficiency.concentration.value * -1;
    faithAttributes.efficiency.initiative.value = faith.disciplines.religiousConcentration.specialities.instantSummoning.lvl * 2;
    faithAttributes.efficiency.resistanceTest.value = faith.disciplines.religiousConcentration.specialities.transcendence.lvl * 2;
  }

  /**
   *
   * @param type
   * @returns {number}
   * @private
   */
  _getMultiplicatorVitnerType(type) {
    switch (type.toLowerCase()) {
      case 'hwitalja':
        return 10;
      case 'morkvitalja':
        return 20;
      case 'vaagritalja':
        return 15;
      default:
        return 0;
    }
  }

  /**
   *
   * @param vitnerAttributes
   * @param vitner
   * @private
   */
  _determineVitnerAttributes(vitnerAttributes, vitner) {
    vitnerAttributes.vitnerReserve.vitner.value = vitner.value;
    vitnerAttributes.vitnerReserve.vitnerCall.value = vitner.disciplines.vitnerCall.lvl * 5;
    vitnerAttributes.vitnerReserve.vitnerHabit.value = vitner.disciplines.vitnerCall.specialities.habitVitner.lvl * 10;
    vitnerAttributes.vitnerReserve.vitnerType.value = vitner.disciplines.vitnerCall.specialities.typeVitner.lvl * this._getMultiplicatorVitnerType(vitner.disciplines.vitnerCall.specialities.typeVitner.toDefine);
    vitnerAttributes.vitnerReserve.total.value = vitnerAttributes.vitnerReserve.vitner.value + vitnerAttributes.vitnerReserve.vitnerCall.value + vitnerAttributes.vitnerReserve.vitnerHabit.value + vitnerAttributes.vitnerReserve.vitnerType.value;
    vitnerAttributes.vitnerWeaving.vitner.value = vitner.value;
    vitnerAttributes.vitnerWeaving.vitnerModelling.value = vitner.disciplines.vitnerModelling.lvl;
    vitnerAttributes.vitnerWeaving.total.value = vitnerAttributes.vitnerWeaving.vitner.value + vitnerAttributes.vitnerWeaving.vitnerModelling.value;
    vitnerAttributes.vitnerWeaving.galda.value = vitner.disciplines.vitnerModelling.specialities.galda.lvl * 2;
    vitnerAttributes.vitnerWeaving.sejda.value = vitner.disciplines.vitnerModelling.specialities.sejda.lvl * 2;
    vitnerAttributes.vitnerWeaving.vyrda.value = vitner.disciplines.vitnerModelling.specialities.vyrda.lvl * 2;
    vitnerAttributes.vitnerWeaving.reinforcementBonus.value = Math.floor(vitner.disciplines.weaverConcentration.specialities.enhancement.lvl * 2);
    vitnerAttributes.efficiency.concentration.value = vitner.disciplines.weaverConcentration.lvl + (vitner.disciplines.weaverConcentration.specialities.carefulWeaving.lvl * 2);
    vitnerAttributes.efficiency.fumble.value = vitnerAttributes.efficiency.concentration.value * -1;
    vitnerAttributes.efficiency.resistanceTest.value = vitner.disciplines.weaverConcentration.specialities.power.lvl * 2;
  }

  /**
   *
   * @param dataset
   * @param valueTest
   * @param spellCost
   * @param spellType
   * @private
   */
  async _rollStats(dataset, valueTest, spellCost, spellType) {
    const context = this.getData();
    const { health, fear } = context.data.data.secondaryTraits;
    const healthModifier = health.modifier;
    const fearModifier = fear.modifier;
    let valueTestModifier = valueTest + healthModifier + fearModifier;
    if (valueTestModifier < 1) valueTestModifier = 1

    const rollInstance = new Roll(dataset.roll || 'd20', this.actor.data.data);
    const roll = await rollInstance.roll();
    const label = dataset.label ? `Test ${dataset.label}` : '';
    const resultRoll = parseInt(roll.total, 10);
    const isSuccess = resultRoll <= valueTestModifier;
    const isCriticalSuccess = resultRoll === 1;
    const isFumble = resultRoll >= 20;
    const description = isCriticalSuccess ?
      game.i18n.localize('TRUDVANG.chat-message.critical-success') : isFumble ?
        game.i18n.localize('TRUDVANG.chat-message.fumble') : isSuccess ?
          game.i18n.localize('TRUDVANG.chat-message.success') : game.i18n.localize('TRUDVANG.chat-message.fail');
    if (spellCost) {
      let reserve = null;
      if (spellType === 'spell') {
        reserve = context.systemData.vitnerAttributes.current.value;
        context.data.data.vitnerAttributes.current.value = reserve - spellCost;
        this.actor.update(context.data);
      } else if (spellType === 'divine') {
        reserve = context.systemData.faithAttributes.current.value;
        context.data.data.faithAttributes.current.value = reserve - spellCost;
        this.actor.update(context.data);
      }
    }
    renderTemplate('/systems/trudvang/templates/chat/skill-test.html', {
      roll: {
        description,
        result: resultRoll,
        label: game.i18n.localize('TRUDVANG.chat-message.roll-label'),
        descriptionLabel: game.i18n.localize('TRUDVANG.chat-message.roll-description'),
        valueTest: valueTestModifier,
      },
    }).then(html => {
      ChatMessage.create({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label,
        roll: roll,
        content: html,
        rollMode: game.settings.get("core", "rollMode"),
        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      });
    });
  }

  /**
   *
   * @param event
   * @private
   */
  async _onRollDamage(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const openRoll = parseInt(dataset.openroll, 10);
    const isDistance = dataset.isdistance !== 'false';
    const label = dataset.label ? `Damage ${dataset.label}` : '';
    const strengthBonus = parseInt(this.actor.data.data.traits.strength.value, 10);
    const rollInstance = new Roll(dataset.damage || 'd10', this.actor.data.data);
    const roll = await rollInstance.roll();
    let rolls = `${roll.result.length > 2 ? `(${roll.result})` : roll.result}`;
    const results = roll.terms[0].results;
    let nbDice = 0;
    const nbFaces = roll.terms[0].faces;
    results.forEach(({ result }) => {
      if (result >= openRoll) nbDice++;
    })
    let finalResult = parseInt(roll.total, 10);
    for (let i=0 ; i < nbDice ; i++) {
      const roll = await this._openRoll(`d${nbFaces}`, openRoll);
      finalResult += roll.finalResult;
      rolls += ` + ${roll.rolls.join(' + ')}`;
    }
    if (!isDistance) {
      rolls += ` + ${strengthBonus}`;
      finalResult += strengthBonus;
    }
    renderTemplate('/systems/trudvang/templates/chat/damage.html', {
      roll: {
        result: `${rolls} = ${finalResult}`,
        label,
        dataset,
      },
    }).then(html => {
      ChatMessage.create({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        roll,
        content: html,
        rollMode: game.settings.get("core", "rollMode"),
        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      });
    });
  }

  async _openRoll(dice, openRoll) {
    let canReRoll = true;
    let finalResult = 0;
    const rolls = [];
    while (canReRoll) {
      const rollInstance = new Roll(dice || 'd10', this.actor.data.data);
      const roll = await rollInstance.roll();
      const resultRoll = parseInt(roll.total, 10);
      rolls.push(resultRoll);
      finalResult += resultRoll;
      canReRoll = resultRoll >= openRoll;
    }
    return { finalResult, rolls };
  }

  /**
   * @param event
   * @private
   */
  _onRollSituation(event) {
    event.preventDefault();
    const context = this.getData();
    const { health, fear } = context.data.data.secondaryTraits;
    const element = event.currentTarget;
    const dataset = element.dataset;
    const traitModifier = parseInt(dataset.modifier, 10) || 0;
    const healthModifier = health.modifier;
    const fearModifier = fear.modifier;

    const dialogData = {
      traitModifier: {
        label: game.i18n.localize('TRUDVANG.situation-rolls.trait-modifier'),
        value: traitModifier,
      },
      healthModifier: {
        label: game.i18n.localize('TRUDVANG.situation-rolls.health-modifier'),
        value: healthModifier,
        state: health.state,
      },
      fearModifier: {
        label: game.i18n.localize('TRUDVANG.situation-rolls.fear-modifier'),
        value: fearModifier,
        state: fear.state,
      },
      modifierLabel: game.i18n.localize('TRUDVANG.situation-rolls.modifier'),
      difficultyLabel: game.i18n.localize('TRUDVANG.situation-rolls.difficulty'),
      testModifier: 0,
      testDifficulty: 10,
      difficulties: [
        {
          label: game.i18n.localize('TRUDVANG.situation-rolls.difficulties.very-difficult'),
          value: 5
        },
        {
          label: game.i18n.localize('TRUDVANG.situation-rolls.difficulties.average'),
          value: 10
        },
        {
          label: game.i18n.localize('TRUDVANG.situation-rolls.difficulties.very-easy'),
          value: 15
        }
      ],
      modifiers: this.modifiers()
    }

    const callback = html => {
      // When dialog confirmed, fill testData dialog information
      // Note that this does not execute until DiceWFRP.prepareTest() has finished and the user confirms the dialog
      const testModifier = Number(html.find('[name="testModifier"]').val());
      const rollModifier = Number(html.find('[name="testDifficulty"]').val());
      const valueTest = rollModifier + testModifier + traitModifier;
      this._rollStats(dataset, valueTest);
    }

    renderTemplate('/systems/trudvang/templates/dialog/dialog-situation-test.html', dialogData)
      .then(html => {
        new Dialog({
          title: game.i18n.localize('TRUDVANG.situation-rolls.title'),
          content: html,
          buttons: {
            rollButton: {
              label: game.i18n.localize('TRUDVANG.situation-rolls.roll-label'),
              callback
            }
          },
          default: 'rollButton'
        }).render(true);
      });
  }

  /**
   *
   * @param event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const context = this.getData();
    const { health, fear } = context.data.data.secondaryTraits;
    const element = event.currentTarget;
    const dataset = element.dataset;
    const sv = parseInt(dataset.sv, 10) || 0;
    const modifier = parseInt(dataset.modifier, 10) || 0;
    const spellModifier = parseInt(dataset.modifierVitnermodelling, 10) || 0;
    const spellCost = parseInt(dataset.cost, 10) || 0;
    const spellType = dataset.spelltype;
    const healthModifier = health.modifier;
    const fearModifier = fear.modifier;

    const dialogData = {
      healthModifier: {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.health-modifier'),
        value: healthModifier,
        state: health.state,
      },
      fearModifier: {
        label: game.i18n.localize('TRUDVANG.classical-Test-rolls.fear-modifier'),
        value: fearModifier,
        state: fear.state,
      },
      modifier: {
        value: modifier,
        label: "Modifier Label"
      },
      modifierLabel: game.i18n.localize('TRUDVANG.classical-Test-rolls.modifier'),
      testModifier: 0,
      modifiers: this.modifiers()
    }

    const callback = html => {
      // When dialog confirmed, fill testData dialog information
      // Note that this does not execute until DiceWFRP.prepareTest() has finished and the user confirms the dialog
      const testModifier = Number(html.find('[name="testModifier"]').val());
      let valueTest = sv + testModifier;
      if (modifier) {
        valueTest = valueTest + modifier;
      }
      if (spellModifier) {
        valueTest = valueTest + spellModifier;
      }
      this._rollStats(dataset, valueTest, spellCost, spellType);
    }

    renderTemplate('/systems/trudvang/templates/dialog/dialog-test.html', dialogData)
      .then(html => {
        new Dialog({
          title: game.i18n.localize('TRUDVANG.classical-Test-rolls.title'),
          content: html,
          buttons: {
            rollButton: {
              label: game.i18n.localize('TRUDVANG.classical-Test-rolls.roll-label'),
              callback
            }
          },
          default: 'rollButton'
        }).render(true);
      });
  }

  /**
   *
   * @param items
   * @private
   */
  _getTablets(items) {
    const tablets = {};
    let hasDivinePowers = false;
    let hasSpells = false;
    items.forEach((item) => {
      if (item.type === "divinePower" || item.type === "spell") {
        hasDivinePowers = hasDivinePowers || item.type === "divinePower";
        hasSpells = hasSpells || item.type === "spell";
        const { tabletName } = item.data;
        if (!tablets[tabletName]) {
          tablets[tabletName] = {
            name: tabletName,
            powers: {}
          }
        }
        tablets[tabletName].powers[item.name] = item;
      }
    });
    return {
      tablets: Object.keys(tablets).length > 0 ? tablets : null,
      divinePower: hasDivinePowers,
      spells: hasSpells,
    };
  }

  /**
   *
   * @param items
   * @param type
   * @returns {*[]|null}
   * @private
   */
  _getItems(items, type) {
    const typeItems = [];
    items.forEach((item) => {
      if (item.type === type ) {
        typeItems.push(item)
      }
    });
    return typeItems.length > 0 ? typeItems : null;
  }

  _calculateInitiativeWithItems(basicInitiative, armours, weapons, shields) {
    const armoursModifierInitiative = armours ? armours.reduce((previousValue, armour) => {
      if (armour.data.equipped) {
        return previousValue + armour.data.initiativeModifier;
      }
      return previousValue;
    }, 0) : 0;
    const weaponsModifierInitiative = weapons ? weapons.reduce((previousValue, weapon) => {
      if (weapon.data.equipped) {
        return previousValue + weapon.data.initiativeModifier;
      }
      return previousValue;
    }, 0) : 0;
    const shieldsModifierInitiative = shields ? shields.reduce((previousValue, shield) => {
      if (shield.data.equipped) {
        return previousValue + shield.data.initiativeModifier;
      }
      return previousValue;
    }, 0) : 0;
    basicInitiative.value += armoursModifierInitiative + weaponsModifierInitiative + shieldsModifierInitiative;
  }

  _calculateNewMovement(initMove, armours) {
    const armoursModifierMvt = armours ? armours.reduce((previousValue, armour) => {
      if (armour.data.equipped) {
        return previousValue + armour.data.movementModifier;
      }
      return previousValue;
    }, 0) : 0;
    initMove.valueUpdated += armoursModifierMvt;
  }
}

/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {

  // Define template paths to load
  const templatePaths = [
    // Attribute list partial.
    "systems/trudvang/templates/actor-parts/description-tab.html",
    "systems/trudvang/templates/actor-parts/skill-knowledge-tab.html",
    "systems/trudvang/templates/actor-parts/skill-wild-tab.html",
    "systems/trudvang/templates/actor-parts/skill-artistic-tab.html",
    "systems/trudvang/templates/actor-parts/skill-agility-tab.html",
    "systems/trudvang/templates/actor-parts/skill-battle-tab.html",
    "systems/trudvang/templates/actor-parts/skill-faith-tab.html",
    "systems/trudvang/templates/actor-parts/skill-vitner-tab.html",
    "systems/trudvang/templates/actor-parts/divine-tablet-tab.html",
    "systems/trudvang/templates/actor-parts/vitner-tablet-tab.html",
    "systems/trudvang/templates/actor-parts/inventory-tab.html",
    "systems/trudvang/templates/actor-parts/trait-tab.html",
    "systems/trudvang/templates/chat/skill-test.html",
    "systems/trudvang/templates/dialog/dialog-situation-test.html",
    "systems/trudvang/templates/items/item-race-sheet.html",
    "systems/trudvang/templates/items/item-skill-sheet.html",
    "systems/trudvang/templates/items/item-divinePower-sheet.html",
    "systems/trudvang/templates/items/item-spell-sheet.html",
    "systems/trudvang/templates/items/item-armour-sheet.html",
    "systems/trudvang/templates/items/item-shield-sheet.html",
    "systems/trudvang/templates/items/item-weapon-sheet.html",
    "systems/trudvang/templates/parts/sheet-attributes.html",
    "systems/trudvang/templates/parts/sheet-groups.html",
    "systems/trudvang/templates/parts/input-personal.html",
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
